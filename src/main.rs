#[macro_use]
extern crate serde_derive;
extern crate serde_xml_rs;

use serde_xml_rs::from_str;

const INPUT: &str = r#"<Book>
<Title>blah blah</Title>
<Authors>
  <Author>
    <FullName>john doe</FullName>
    <Email>john@thedoes.com</Email>
  </Author>
  <Author>
    <FullName>jane doe</FullName>
    <Email>jane@thedoes.com</Email>
  </Author>
</Authors></Book>"#;

#[derive(Debug, Deserialize)]
pub struct Book {
    #[serde(rename = "Title")]
    pub title: String,
    // How do I tell serde to create an Author for
    // each Author element in the Authors element?
    #[serde(rename = "Authors")]
    pub authors: Vec<Author>,
}

#[derive(Debug, Deserialize)]
pub struct Author {
    #[serde(rename = "FullName")]
    name: String,
    #[serde(rename = "Email")]
    email: String,
}

fn main() {
  let book: Book = from_str(INPUT).unwrap();
  println!("{:?}", book);
}
